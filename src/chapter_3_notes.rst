Standart entities:
--------------------
	-User

*What user owns*
	-Projects


Projects
----------

:Project: is an object which represents a physical location. Representation of the project contains widgets (which are graphical representation of some elements), schema (background image).

Project properties
	-name
	-description
	-is_public
	-schema
	-project_id (for linking with MQ)

Project relationships:
	-user (owner) FK
	-users (allowed users) M2M
      
      
Widgets
-----------

:Widget: represents real object on the project's schema. Also widget has a set of properties which are interpreted just on frontend.

Widget properties:
	-name
	-description
	-element_id  (for linking with MQ)

Widget relationships:
	-project FK
	-element FK


Element
----------

:Element: basicaly, this is a template for widgets. Element has icon, states. But it isn't linked to a project.

Element properties:
	-name
	-description
	-is_public
	-icon

Element relationships:
	-user (owner) FK
      
      
ElementState
-------

:ElementState: represents state of a element. State is represented by an icon or class.

ElementState properties:
	-name
	-code (int. For easier communication and displaying on charts)
	-icon ??
	-classes ??
    
ElementState relationships:
	-element FK


WidgetPropertyValue
----------------

:WidgetPropertyValue: actually is a M2M table. Just holds a value which can be converted to a required type due to WidgetPropertyType.

WidgetPropertyValue properties:
	-value (str)

WidgetPropertyValue relationships:
	-widget_property FK
	-widget FK


WidgetProperty
-------------------

:WidgetProperty: is a type of widget property.

WidgetProperty properties:
	-name

WidgetProperty relationships:
	-data_type FK


DataType
----------

:DataType: holds data types.

DataType properties:
	-name