#CREATIVE COMMONS

##Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)

This license lets others remix, tweak, and build upon this work non-commercially,
and although their new works must also acknowledge the creator and be non-commercial,
they donâ€™t have to license their derivative works on the same terms.

[View License Deed](http://creativecommons.org/licenses/by-nc/4.0/) | [View Legal Code](http://creativecommons.org/licenses/by-nc/4.0/legalcode)

For other Creative Commons Licenses, see http://creativecommons.org/licenses/